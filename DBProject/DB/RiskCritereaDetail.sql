
/****** Object:  Table [dbo].[RiskCritereaDetail]    Script Date: 5/13/2021 2:15:49 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RiskCritereaDetail]') AND type in (N'U'))
DROP TABLE [dbo].[RiskCritereaDetail]
GO

/****** Object:  Table [dbo].[RiskCritereaDetail]    Script Date: 5/13/2021 2:15:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RiskCritereaDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Parent] [int] NULL,
	[RiskCriteria] [nvarchar](300) NULL,
	[RiskLable] [int] NULL,
	[CriteriaValue] [int] NULL,
	[MinValue] [nvarchar](50) NULL,
	[MaxValue] [nvarchar](50) NULL,
 CONSTRAINT [PK_RiskCritereaDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


