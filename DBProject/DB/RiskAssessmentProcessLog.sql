
/****** Object:  Table [dbo].[AssessmentProcessLog]    Script Date: 5/22/2021 8:19:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RiskAssessmentProcessLog](
  [Id] [int] IDENTITY(1,1) NOT NULL,
  [CustomerId] [int] NULL,
  [AssessmentId] [int] NULL,
  [Source] [nvarchar](200) NULL,
  [Info] [nchar](1000) NULL,
  [Warning] [nvarchar](1000) NULL,
  [Error] [nvarchar](1000) NULL,
  [CenterId] [int] NULL,
  [CenterName] [nvarchar](50) NULL,
  [AssessedBy] [nvarchar](50) NULL,
  [CustomerNote] [nvarchar](2000) NULL,
 CONSTRAINT [PK_RiskAssessmentProcessLog] PRIMARY KEY CLUSTERED 
(
  [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO