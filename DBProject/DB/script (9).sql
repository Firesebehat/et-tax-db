
/****** Object:  Table [dbo].[VATDeclaration]    Script Date: 5/12/2021 4:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP Table VATDeclaration
GO 
CREATE TABLE [dbo].[VATDeclaration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaxpayerId] [int] NOT NULL,
	[Tin] [nvarchar](50) NULL,
	[FiscalYear] [int] NOT NULL,
	[Month] [int] NULL,
	[DeclarationDate] [datetime] NOT NULL,
	[VATnumber] [nvarchar](50) NULL,
	[TaxableGoodsAndSupplies] [float] NULL,
	[TaxableGoodsAndSuppliesVAT] [float] NULL,
	[ZeroRatedGoods] [float] NULL,
	[ExemptSuppliesOrSales] [float] NULL,
	[RemittedSupplies] [float] NULL,
	[TotalSuppliesOrSales] [float] NULL,
	[MonthlyVAT] [float] NULL,
	[LocalPurchasesInput] [float] NULL,
	[LocalPurchasesInputVAT] [float] NULL,
	[ImportedInputs] [float] NULL,
	[ImportedInputsVAT] [float] NULL,
	[GeneralExpenseInputs] [float] NULL,
	[GeneralExpenseInputsVAT] [float] NULL,
	[PurchaseswithnoVATorUnclaimedInputs] [float] NULL,
	[TotalInputs] [float] NULL,
	[TaxableInputs] [float] NULL,
	[TotalInputsVAT] [float] NULL,
	[MonthlyNetVATDue] [float] NULL,
	[MonthlyOtherCredit] [float] NULL,
	[CreditFromPeriviousMonth] [float] NULL,
	[TotalAmountToPaid] [float] NULL,
	[TaxableGoodsAndSuppliesRevised] [float] NULL,
	[TaxableGoodsAndSuppliesVATRevised] [float] NULL,
	[ZeroRatedGoodsRevised] [float] NULL,
	[ExemptSuppliesOrSalesRevised] [float] NULL,
	[RemittedSuppliesRevised] [float] NULL,
	[TotalSuppliesOrSalesRevised] [float] NULL,
	[MonthlyVATRevised] [float] NULL,
	[LocalPurchasesInputRevised] [float] NULL,
	[LocalPurchasesInputVATRevised] [float] NULL,
	[ImportedInputsRevised] [float] NULL,
	[ImportedInputsVATRevised] [float] NULL,
	[GeneralExpenseInputsRevised] [float] NULL,
	[GeneralExpenseInputsVATRevised] [float] NULL,
	[PurchaseswithnoVATorUnclaimedInputsRevised] [float] NULL,
	[TotalInputsRevised] [float] NULL,
	[TaxableInputsRevised] [float] NULL,
	[TotalInputsVATRevised] [float] NULL,
	[MonthlyNetVATDueRevised] [float] NULL,
	[MonthlyOtherCreditRevised] [float] NULL,
	[CreditFromPeriviousMonthRevised] [float] NULL,
	[TotalAmountToPaidRevised] [float] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CenterId] [int] NOT NULL,
	[CenterName] [nvarchar](250) NOT NULL,
	[CreatedUserId] [nvarchar](250) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedUserId] [nvarchar](250) NULL,
	[Url] [nvarchar](100) NULL,
	[IsAudit] [bit] NULL,
	[AuditedDate] [datetime] NULL,
	[AuditedBy] [nvarchar](50) NULL,
	[AuditedUserId] [nvarchar](250) NULL,
	[AuditId] [int] NULL,
	[IsAssessed] [bit] NULL,
	[LocalPurchasesInputNON] [float] NULL,
	[DeductibleVATReverseTaxationNON] [float] NULL,
	[TaxAdjustmentDebitBuyersNON] [float] NULL,
	[TaxAdjustmentCreditBuyersNON] [float] NULL,
	[ImportedInputsNON] [float] NULL,
	[PurchaseswithnoVATorUnclaimedInputsNON] [float] NULL,
	[VATGovernmentVoucher] [float] NULL,
	[CreditForNextMonth] [float] NULL,
	[SuppliesSubjectReverseTaxation] [float] NULL,
	[TaxAjestmentWithDebit] [float] NULL,
	[TaxAdjustmentCreditSuppliers] [float] NULL,
	[TotalSalesSupplies] [float] NULL,
 CONSTRAINT [PK_VAT_Declaration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VATDeclaration]  WITH CHECK ADD  CONSTRAINT [FK_VATDeclaration_TaxCenter] FOREIGN KEY([CenterId])
REFERENCES [dbo].[TaxCenter] ([Id])
GO
ALTER TABLE [dbo].[VATDeclaration] CHECK CONSTRAINT [FK_VATDeclaration_TaxCenter]
GO
