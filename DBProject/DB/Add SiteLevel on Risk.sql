﻿alter table [dbo].[TaxAuditFilter] add SiteLevel int


DROP Table [FinancialCalender]
GO
/****** Object:  Table [dbo].[FinancialCalender]    Script Date: 5/25/2021 1:29:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialCalender](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CalenderNameEng] [nvarchar](50) NULL,
	[CalenderNameAmh] [nchar](50) NULL,
	[StartDateCG] [datetime] NULL,
	[EndDateGC] [datetime] NULL,
	[FiscalYear] [int] NULL,
	[Code] [int] NULL,
 CONSTRAINT [PK_Calender] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FinancialCalenderDetail]    Script Date: 5/25/2021 1:29:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialCalenderDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[parent] [int] NULL,
	[StartDateCG] [datetime] NULL,
	[EndDateGC] [datetime] NULL,
	[FiscalYear] [int] NULL,
 CONSTRAINT [PK_FinancialCalenderDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[FinancialCalender] ON 

INSERT [dbo].[FinancialCalender] ([Id], [CalenderNameEng], [CalenderNameAmh], [StartDateCG], [EndDateGC], [FiscalYear], [Code]) VALUES (1, N'Ethiopian Calender', N'የኢትዮጵያ መቁጠሪያ                                      ', CAST(N'2019-07-08T00:00:00.000' AS DateTime), CAST(N'2020-07-07T00:00:00.000' AS DateTime), 2012, 1)
INSERT [dbo].[FinancialCalender] ([Id], [CalenderNameEng], [CalenderNameAmh], [StartDateCG], [EndDateGC], [FiscalYear], [Code]) VALUES (2, N'Gregorian Calender', N'ግሪጎሪያን መቁጠሪያ                                      ', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime), 2012, 1)
INSERT [dbo].[FinancialCalender] ([Id], [CalenderNameEng], [CalenderNameAmh], [StartDateCG], [EndDateGC], [FiscalYear], [Code]) VALUES (3, N'Chainis Calander', N'Chainis Calander                                  ', CAST(N'2019-04-01T00:00:00.000' AS DateTime), CAST(N'2020-03-31T00:00:00.000' AS DateTime), 2012, 1)
INSERT [dbo].[FinancialCalender] ([Id], [CalenderNameEng], [CalenderNameAmh], [StartDateCG], [EndDateGC], [FiscalYear], [Code]) VALUES (5, N'Ethiopian Calender', N'የኢትዮጵያ መቁጠሪያ                                      ', CAST(N'2019-07-08T00:00:00.000' AS DateTime), CAST(N'2020-07-07T00:00:00.000' AS DateTime), 2013, NULL)
INSERT [dbo].[FinancialCalender] ([Id], [CalenderNameEng], [CalenderNameAmh], [StartDateCG], [EndDateGC], [FiscalYear], [Code]) VALUES (6, N'Gregorian Calender', N'ግሪጎሪያን መቁጠሪያ                                      ', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime), 2013, NULL)
INSERT [dbo].[FinancialCalender] ([Id], [CalenderNameEng], [CalenderNameAmh], [StartDateCG], [EndDateGC], [FiscalYear], [Code]) VALUES (7, N'Chainis Calander', N'Chainis Calander                                  ', CAST(N'2019-04-01T00:00:00.000' AS DateTime), CAST(N'2020-03-31T00:00:00.000' AS DateTime), 2013, NULL)
SET IDENTITY_INSERT [dbo].[FinancialCalender] OFF
GO
SET IDENTITY_INSERT [dbo].[FinancialCalenderDetail] ON 

INSERT [dbo].[FinancialCalenderDetail] ([Id], [parent], [StartDateCG], [EndDateGC], [FiscalYear]) VALUES (1, 1, CAST(N'2019-07-08T00:00:00.000' AS DateTime), CAST(N'2020-07-07T00:00:00.000' AS DateTime), 2012)
INSERT [dbo].[FinancialCalenderDetail] ([Id], [parent], [StartDateCG], [EndDateGC], [FiscalYear]) VALUES (2, 2, CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime), 2012)
INSERT [dbo].[FinancialCalenderDetail] ([Id], [parent], [StartDateCG], [EndDateGC], [FiscalYear]) VALUES (3, 3, CAST(N'2019-04-01T00:00:00.000' AS DateTime), CAST(N'2020-03-31T00:00:00.000' AS DateTime), 2012)
INSERT [dbo].[FinancialCalenderDetail] ([Id], [parent], [StartDateCG], [EndDateGC], [FiscalYear]) VALUES (4, 1, CAST(N'2019-07-08T00:00:00.000' AS DateTime), CAST(N'2020-07-07T00:00:00.000' AS DateTime), 2013)
INSERT [dbo].[FinancialCalenderDetail] ([Id], [parent], [StartDateCG], [EndDateGC], [FiscalYear]) VALUES (5, 2, CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2020-12-31T00:00:00.000' AS DateTime), 2013)
INSERT [dbo].[FinancialCalenderDetail] ([Id], [parent], [StartDateCG], [EndDateGC], [FiscalYear]) VALUES (6, 3, CAST(N'2019-04-01T00:00:00.000' AS DateTime), CAST(N'2020-03-31T00:00:00.000' AS DateTime), 2013)
SET IDENTITY_INSERT [dbo].[FinancialCalenderDetail] OFF
GO
